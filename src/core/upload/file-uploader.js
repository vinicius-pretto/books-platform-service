const multer = require('multer');
const path = require('path');
const uuid = require('uuid/v4');
const config = require('../../../config');

class FileUploader {
  constructor() {
    this.storage = multer.diskStorage({
      destination: (req, file, done) => {
        done(null, config.upload.folder);
      },
      filename: (req, file, done) => {
        const fileName = `${uuid()}${path.extname(file.originalname)}`;
        done(null, fileName);
      }
    });
  }

  uploadFile(inputName) {
    const upload = multer({ storage: this.storage });
    return upload.single(inputName);
  }
}

module.exports = FileUploader;
