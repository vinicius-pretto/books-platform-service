const HttpStatus = require('http-status');
const config = require('../../config');

const cors = (req, res, next) => {
  const allowedHeaders = config.cors.allowedHeaders.join(',');
  const allowedDomains = config.cors.allowedDomains.join(',');
  const allowedMethods = config.cors.allowedMethods.join(',');
  const originDomain = req.headers.origin;

  if (allowedDomains.includes(originDomain)) {
    res.header('Access-Control-Allow-Origin', originDomain);
    res.header('Access-Control-Allow-Headers', allowedHeaders);
    res.header('Access-Control-Allow-Methods', allowedMethods);

    if (req.method === 'OPTIONS') {
      return res.sendStatus(HttpStatus.OK);
    }
    return next();
  }
  next();
};

module.exports = cors;
