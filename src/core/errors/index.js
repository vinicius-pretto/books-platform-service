const uploadFileError = require('./upload-file.error');
const internalServerError = require('./internal-server.error');
const validationError = require('./validation.error');
const unauthorizedError = require('./unauthorized.error');

const errors = {
  uploadFileError,
  internalServerError,
  validationError,
  unauthorizedError
};

module.exports = errors;
