const HttpStatus = require('http-status');
const ErrorCode = require('./error-code');

function validationError({ stack }, detail) {
  const error = new Error();
  error.code = ErrorCode.VALIDATION_ERROR;
  error.status = HttpStatus.BAD_REQUEST;
  error.message = 'Validation Error';
  error.detail = detail;

  return {
    body: error,
    stack
  };
}

module.exports = validationError;
