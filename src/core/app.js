const express = require('express');
const helmet = require('helmet');
const compression = require('compression');
const config = require('../../config');
const Router = require('./router');
const errorHandler = require('./error-handler');
const cors = require('./cors');

const app = express();

app.use(cors);
app.use(compression());
app.use(helmet());
app.use(express.json());
app.use(config.upload.path, express.static(config.upload.folder));
Router.registerRoutes(app);
app.use(errorHandler);

module.exports = app;
