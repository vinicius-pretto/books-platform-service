const logger = require('./logger').createLogger(__filename);

// eslint-disable-next-line no-unused-vars
function errorHandler(error, req, res, next) {
  logger.error({ req }, error.stack);
  res.status(error.body.status).json({ error: error.body });
}

module.exports = errorHandler;
