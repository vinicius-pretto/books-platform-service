const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('../../swagger.json');
const config = require('../../config');
const upload = require('./upload');

class Router {
  static registerRoutes(app) {
    app.use(`${config.apiVersion}/api-docs`, swaggerUi.serve, swaggerUi.setup(swaggerDocument));
    app.use(config.apiVersion, upload);
  }
}

module.exports = Router;
