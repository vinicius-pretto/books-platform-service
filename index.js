const app = require('./src/core/app');
const config = require('./config');
const logger = require('./src/core/logger').createLogger('index.js');

app.listen(config.port, () => {
  logger.info(`Server is listening at port ${config.port}`);
});
