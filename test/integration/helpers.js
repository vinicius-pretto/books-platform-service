const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../../src/core/app');

chai.use(chaiHttp);

global.request = chai.request(app).keepOpen();
global.expect = chai.expect;
