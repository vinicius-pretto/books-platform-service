/* eslint-disable no-undef */
const fs = require('fs');
const path = require('path');
const HttpStatus = require('http-status');
const rimraf = require('rimraf');
const config = require('../../../config');

const UPLOADS_FOLDER = config.upload.folder;

describe('Upload', () => {
  context('POST /v1/upload', () => {
    const file = {
      src: path.resolve('test/integration/upload/file.pdf'),
      name: 'file.pdf'
    };

    context('Successfull', () => {
      beforeEach(done => {
        rimraf(UPLOADS_FOLDER, () => {
          fs.mkdir(UPLOADS_FOLDER, () => {
            done();
          });
        });
      });

      afterEach(done => {
        rimraf(UPLOADS_FOLDER, () => {
          done();
        });
      });

      it('should upload the file', async () => {
        const response = await request
          .post('/v1/upload')
          .attach('file', fs.readFileSync(file.src), file.name);

        expect(response.status).to.be.eql(HttpStatus.CREATED);
        expect(response.body.file.name).to.be.eql('file.pdf');
        expect(response.body.file.size).to.be.eql(10790);
        expect(response.body.file.mimetype).to.be.eql('application/pdf');
        expect(response.body.file.src).to.exist;
      });
    });

    context('Failure', () => {
      context("There isn't uploads folder", () => {
        beforeEach(done => {
          rimraf(UPLOADS_FOLDER, () => {
            done();
          });
        });

        it('should return Internal Server Error', async () => {
          const response = await request
            .post('/v1/upload')
            .attach('file', fs.readFileSync(file.src), file.name);

          const expectedError = {
            code: 'UPLOAD_FILE_ERROR',
            status: HttpStatus.INTERNAL_SERVER_ERROR,
            message: 'Internal Server Error',
            detail: 'Failed to upload file'
          };

          expect(response.status).to.be.eql(HttpStatus.INTERNAL_SERVER_ERROR);
          expect(response.body.error).to.be.eql(expectedError);
        });
      });
    });
  });
});
