# Books Platform Service

## Requiments

- [Node.js](https://nodejs.org/en/)
- [NPM](https://www.npmjs.com/)

## Running Application

### Install Dependencies

```
$ npm install
```

### Start Application

```
$ npm start
```

## Run Tests

```
$ npm test
```

## [Swagger (API docs)](https://swagger.io/)

```
http://localhost:8080/v1/api-docs
```
